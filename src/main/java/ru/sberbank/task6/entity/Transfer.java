package ru.sberbank.task6.entity;

import ru.sberbank.task6.annotation.MaxMin;
import ru.sberbank.task6.annotation.MaxMinLength;
import ru.sberbank.task6.annotation.NotEmpty;
import ru.sberbank.task6.annotation.NotNull;

import java.util.UUID;

public class Transfer {

    @MaxMin
    @NotNull
    @NotEmpty
    private int id;

    @NotNull
    @NotEmpty
    private Client firstClient;

    @NotNull
    @NotEmpty
    private Client lastClient;

    @NotNull
    @NotEmpty
    private int summ;

    public Transfer(Client firstClient, Client lastClient, int summ) {
        this.firstClient = firstClient;
        this.lastClient = lastClient;
        this.summ = summ;
        id = UUID.randomUUID().hashCode();
    }

    public int getId() {
        return id;
    }

    public Client getFirstClient() {
        return firstClient;
    }

    public Client getLastClient() {
        return lastClient;
    }

    public int getSumm() {
        return summ;
    }
}
