package ru.sberbank.task6.entity;

import ru.sberbank.task6.annotation.MaxMin;
import ru.sberbank.task6.annotation.MaxMinLength;
import ru.sberbank.task6.annotation.NotEmpty;
import ru.sberbank.task6.annotation.NotNull;

import java.util.UUID;

public class Client {

    @MaxMinLength
    @NotNull
    @NotEmpty
    private String lastName;

    @MaxMinLength
    @NotNull
    @NotEmpty
    private String firstName;

    @MaxMin
    @NotNull
    @NotEmpty
    private int id;

    public Client(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.id = UUID.randomUUID().hashCode();
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }
}
