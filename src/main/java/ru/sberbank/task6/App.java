package ru.sberbank.task6;

import ru.sberbank.task6.entity.Client;
import ru.sberbank.task6.entity.Transfer;
import ru.sberbank.task6.impl.ValidatorService;

public class App {
    public static void main(String[] args) throws Exception {

        Client client1 = new Client("Sidorov", "Ivan");
        Client client2 = new Client("Fiborin", "Denis");

        Transfer transfer = new Transfer(client1, client2, 5500);

        ValidatorService validator = new ValidatorService();

        validator.validationMaxMinLength(client1);

    }
}
