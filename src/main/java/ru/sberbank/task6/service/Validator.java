package ru.sberbank.task6.service;

public interface Validator {

    void validationMaxMin(Object o) throws Exception;

    void validationMaxMinLength(Object o) throws Exception;

    void validationNotEmpty(Object o) throws Exception;

    void validationNotNull(Object o) throws Exception;

}
