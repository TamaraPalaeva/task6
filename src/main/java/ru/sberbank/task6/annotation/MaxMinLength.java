package ru.sberbank.task6.annotation;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface MaxMinLength {
    int max = 20;
    int min = 8;
}
