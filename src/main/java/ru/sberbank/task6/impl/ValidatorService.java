package ru.sberbank.task6.impl;

import ru.sberbank.task6.annotation.MaxMin;
import ru.sberbank.task6.annotation.MaxMinLength;
import ru.sberbank.task6.annotation.NotEmpty;
import ru.sberbank.task6.annotation.NotNull;
import ru.sberbank.task6.service.Validator;

import java.lang.reflect.Field;

public class ValidatorService implements Validator {

    public void validationMaxMin(Object o) throws Exception{
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if (field.isAnnotationPresent(MaxMin.class)){
                MaxMin an = field.getAnnotation(MaxMin.class);
                int min = an.min;
                int max = an.max;

                int value = Integer.parseInt(field.get(o).toString());
                if ((value < min) || (value > max)){
                    throw new IllegalStateException(field.getName() +
                            " параметр должен быть между "
                            + min + " и " + max);
                }
            }
        }
    }

    public void validationMaxMinLength(Object o) throws Exception{
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if (field.isAnnotationPresent(MaxMinLength.class)){
                MaxMinLength an = field.getAnnotation(MaxMinLength.class);
                int min = an.min;
                int max = an.max;

                 String value = field.get(o).toString();
                    System.out.println(value.toString());

                if ((value.length() < min) || (value.length() > max)){
                    throw new IllegalStateException(field.getName() +
                            " количество символов параметра должна быть между "
                            + min + " и " + max);
                }
            }
        }
    }

    public void validationNotEmpty(Object o) throws Exception{
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if (field.isAnnotationPresent(NotEmpty.class)){

                String value = field.get(o).toString();
                if ((value.isEmpty())){
                    throw new IllegalStateException(field.getName() +
                            " параметр не должен быть пустым");
                }
            }
        }
    }

    public void validationNotNull(Object o) throws Exception{
        Class<?> clazz = o.getClass();
        for (Field field : clazz.getDeclaredFields()){
            field.setAccessible(true);
            if (field.isAnnotationPresent(NotNull.class)){

                String value = field.get(o).toString();
                if ((value.isEmpty())){
                    throw new IllegalStateException(field.getName() +
                            " параметр не должен быть null");
                }
            }
        }
    }
}
